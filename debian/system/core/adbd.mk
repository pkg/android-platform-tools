NAME = adbd

SOURCES = \
  daemon/main.cpp \

SOURCES := $(foreach source, $(SOURCES), system/core/adb/$(source))
OBJECTS = $(SOURCES:.cpp=.$(NAME).o)

CXXFLAGS += -std=gnu++2a
CPPFLAGS += \
  -D_GNU_SOURCE \
  -DADB_VERSION='"$(DEB_VERSION)"' \
  -DADB_HOST=0 \
  -DALLOW_ADBD_NO_AUTH=1 \
  -I/usr/include/android \
  -Isystem/core/adb \
  -Isystem/core/base/include \
  -Isystem/core/include \

LDFLAGS += \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out/system/core \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -fuse-ld=gold \
  -lbase \
  -lcrypto \
  -lcutils \
  -lresolv \
  -lpthread \
  -llog \
  -lsystemd \
  -lusb-1.0 \
  -pie \

STATIC_LIBS = \
  debian/out/system/core/libadbd.a \
  debian/out/system/core/libcrypto_utils.a \

# -latomic should be the last library specified
# https://github.com/android/ndk/issues/589
ifneq ($(filter armel mipsel,$(DEB_HOST_ARCH)),)
  LDFLAGS += -latomic
endif

debian/out/system/core/$(NAME): $(OBJECTS)
	$(CXX) -o $@ $^ $(STATIC_LIBS) $(LDFLAGS)

$(OBJECTS): %.$(NAME).o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
