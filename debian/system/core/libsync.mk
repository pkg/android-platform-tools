NAME = libsync

SOURCES = \
  sync.c \

SOURCES := $(foreach source, $(SOURCES), system/core/libsync/$(source))
OBJECTS = $(SOURCES:.c=.$(NAME).o)

CPPFLAGS += \
  -D__ANDROID_API__=29 \
  -D"__INTRODUCED_IN(x)=" \
  -Isystem/core/include \
  -Isystem/core/libsync/include \

LDFLAGS += \
  -Ldebian/out/system/core \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Wl,-soname,$(NAME).so.0 \
  -lbsd \
  -shared

build: $(OBJECTS)
	$(CC) $^ -o debian/out/system/core/$(NAME).so.0 $(LDFLAGS)
	ln -sf $(NAME).so.0 debian/out/system/core/$(NAME).so

$(OBJECTS): %.$(NAME).o: %.c
	$(CC) -c -o $@ $< $(CPPFLAGS) $(CFLAGS)
