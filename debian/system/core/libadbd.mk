NAME := libadbd

LIBADBD_SRC_FILES := \
  daemon/auth.cpp \
  daemon/jdwp_service.cpp \

LIBADBD_posix_srcs := \
  sysdeps_unix.cpp \
  sysdeps/posix/network.cpp \

LIBADBD_linux_SRC_FILES := \
  adb.cpp \
  adb_io.cpp \
  adb_listeners.cpp \
  adb_trace.cpp \
  adb_unique_fd.cpp \
  adb_utils.cpp \
  services.cpp \
  shell_service_protocol.cpp \
  sockets.cpp \
  socket_spec.cpp \
  transport.cpp \
  transport_local.cpp \
  transport_usb.cpp \
  types.cpp \
  adbconnection/adbconnection_server.cpp \
  fdevent/fdevent.cpp \
  fdevent/fdevent_epoll.cpp \
  daemon/file_sync_service.cpp \
  daemon/services.cpp \
  daemon/shell_service.cpp \
  daemon/usb.cpp \
  daemon/usb_ffs.cpp \
  daemon/usb_legacy.cpp \
  sysdeps/errno.cpp \

LOCAL_SRC_FILES := \
  $(LIBADBD_SRC_FILES) \
  $(LIBADBD_posix_srcs) \
  $(LIBADBD_linux_SRC_FILES) \

GEN := debian/out/system/core/transport_mdns_unsupported.cpp

LIBADBD_CORE_SRC_FILES := \
  diagnose_usb/diagnose_usb.cpp \
  libasyncio/AsyncIO.cpp \

LIBADBD_AUTH_SRC_FILES := \
  frameworks/native/libs/adbd_auth/adbd_auth.cpp \

SOURCES := $(foreach source, $(LOCAL_SRC_FILES), adb/$(source)) $(LIBADBD_CORE_SRC_FILES)
SOURCES := $(foreach source, $(SOURCES), system/core/$(source)) $(LIBADBD_AUTH_SRC_FILES) $(GEN)
OBJECTS = $(SOURCES:.cpp=.$(NAME).o)

CXXFLAGS += -std=gnu++2a
CPPFLAGS += \
  -DPLATFORM_TOOLS_VERSION='"$(PLATFORM_TOOLS_VERSION)"' \
  -DADB_VERSION='"$(DEB_VERSION)"' \
  -DADB_HOST=0 \
  -DPAGE_SIZE=4096 \
  -I/usr/include/android \
  -Isystem/core/adb/daemon/include \
  -Isystem/core/adb \
  -Isystem/core/adb/adbconnection/include \
  -Isystem/core/base/include \
  -Isystem/core/diagnose_usb/include \
  -Isystem/core/include \
  -Isystem/core/libasyncio/include \
  -Isystem/core/libcrypto_utils/include \
  -Isystem/core/libcutils/include \
  -Isystem/core/fs_mgr/include \
  -Iframeworks/native/libs/adbd_auth/include/ \
  -Wno-narrowing \

debian/out/system/core/$(NAME).a: $(OBJECTS)
	$(AR) -rcs $@ $^

$(OBJECTS): %.$(NAME).o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

debian/out/system/core/transport_mdns_unsupported.cpp:
	mkdir -p debian/out/system/core/
	echo 'void init_mdns_transport_discovery(void) {}' > $@
